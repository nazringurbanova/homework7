//package com.company;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import java.util.Arrays;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//    class FamilyTest {
//    public Family family;
//   Human mother;
//   Human father;
//   Human human1;
//   Human human2;
//    com.company.Pet pet;
//
//    @BeforeEach
//    void familyInfo() {
//        Human mother = new Human("Anna", "Golden", 1988,100);
//        Human father = new Human("John", "Black", 1988,120);
//        family = new Family(mother, father);
//        Human human1 = new Human("name1", "surname1", 1999,40);
//        family.addChild(human1);
//        Human human2 = new Human("name2", "surname2", 1998,50);
//        family.addChild(human2);
//        Human human3 = new Human("name3", "surname3", 1998,50);
//        family.addChild(human3);
//    }
//
//    @Test
//    void testToString() {
//        String expected = "com.Family{" +
//                "mother=" + mother +
//                ", father=" + father +
//                ", children=" + Arrays.deepToString(family.children) +
//                ", pet=" + pet +
//                '}';
//        Assertions.assertEquals(family.toString(), expected);
//    }
//
//    @Test
//    void addChild() {
//        family.addChild(human1);
//        assertTrue(((family.get_Children()).contains((human1))));
//    }
//
//    @Test
//    void deleteChild() {
//        assertTrue(family.deleteChild(1));
//        assertFalse(family.get_Children().contains(human2));
//    }
//
//    @Test
//    void deleteNonExistingChild() {
//        assertFalse(family.deleteChild(23));
//    }
//
//    @Test
//    void countFamily() {
//        Assertions.assertEquals(5,family.countFamily());
//    }
//    @Test
//    void countFalseFamily() {
//            Assertions.assertEquals(5,family.countFamily());
//        }
//
//
//
//}