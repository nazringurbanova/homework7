package com.company;

public class DomesticCat extends Pet {

    @Override
    public void respond() {
        System.out.println("Hello owner, I am a domestic cat");
    }

    @Override
    public void foul() {
        System.out.println("Domestic cat");

    }
}
