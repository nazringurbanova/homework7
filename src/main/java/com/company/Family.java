package com.company;

import java.util.Arrays;
import java.util.Objects;
import java.util.ArrayList;

public class
Family {

    private Human mother;
    private Human father;
    private Human[] children = new com.company.Human[0];
    private Pet pet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                '}';
    }

    public Family(com.company.Human mother, com.company.Human father, com.company.Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public void addChild(Human human) {
        Human[] child = new Human[children.length + 1];
        for (int j = 0; j < children.length; j++) {
            child[j] = children[j];
        }
        child[child.length - 1] = human;
        this.children = child;

    }

    public boolean deleteChild(int i) {
        boolean child = false;
        int length = children.length;
        Human[] human = new Human[length - 1];
        int l = 0;
        for (int a = 0; a < length; a++) {
            if (a != l) {
                human[l++] = children[a];
            }
        }
        if (i < length) {
            child = true;
        } else {
            return false;
        }
        this.children = human;

        return child;


    }


    public int countFamily() {
        return children.length + 2;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father);

    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    public ArrayList<Human> get_Children() {
        ArrayList<Human> children_List = new ArrayList<>();
        int length = children.length;

        for (int i = 0; i < length; i++) {
            children_List.add(children[i]);
        }
        return children_List;
    }

}