package com.company;

public abstract class Pet{
    private String species;
    private String nickname;
    private int age;

    private int trickLevel;
    String[] habits = new String[99999];

    public Pet(String species,String nickname){
        this.species=species;
        this.nickname=nickname;
    }

    public Pet(){
        this.species = "No data";
        this.nickname = "No data";
        this.age = 0;
        this.trickLevel = 0;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void setSpecies(String species){
        this.species = species;
    }

    public String getSpecies(){
        return this.species;
    }

    public void setnickname(String nickname){
        this.nickname = nickname;
    }

    public String getnickname(){
        return this.nickname;
    }

    public void setage(int age){
        this.age = age;
    }

    public int getage(){
        return this.age;
    }

    public void settrickLevel(int trickLevel){
        this.trickLevel = trickLevel;
    }

    public int gettrickLevel(){
        return this.trickLevel;
    }

    public void eat(){
        System.out.println("I am eating");
    }

    public abstract void respond();//different codes will be written in the inherited classes
    public abstract void foul();

    public String toString() {
        return ("dog{nickname = 'Rock',age=5,trickLevel=75,habits=[eat,drink,sleep]}");
    }



}