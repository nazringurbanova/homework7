package com.company;

public final class Woman extends Human{
    @Override
    public void greetPet() {
        System.out.println("Hello, pet, I am your female owner");
    }

    public void makeup(){
        System.out.println("I am a makeup artist");
    }

}

