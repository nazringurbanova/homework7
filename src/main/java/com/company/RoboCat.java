package com.company;

public class RoboCat extends Pet{
    @Override
    public void respond() {
        System.out.println("Hello owner, I am a robot cat");
    }

    @Override
    public void foul() {
        System.out.println("Robot cat");

    }
}
